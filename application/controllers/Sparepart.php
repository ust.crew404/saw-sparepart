<?php
/**
* 
*/
class Sparepart extends CI_Controller
{
	
	function __construct()
	{
		# code...
		parent::__construct();
		if($this->session->userdata('login') != 'login_admin')
				{
					redirect(base_url());
				}
				$this->load->model('Login_m');
	}
	function index(){
        $data['title'] = "SPK dengan SAW";
        $data['menu'] = $this->Login_m->menu();
        $content['kategori'] = $this->db->get('tbl_sparepart')->result();
		$data['content'] = $this->load->view('dashboard/list_sparepart',$content,true);
		$this->load->view('dashboard/index',$data);
    }
    function tambah(){
        $merk = $this->input->post('merk');
        $jenis = $this->input->post('jenis');
        $sparepart = $this->input->post('sparepart');
        $harga = $this->input->post('harga');
    	$garansi = $this->input->post('garansi');

        $data = array(
                        'merk' => $merk,
                        'jenis' => $jenis,
                        'sparepart' => $sparepart,
                        'harga' => $harga,
                        'garansi' => $garansi
                     );
    	$this->db->insert('tbl_sparepart', $data);
            $this->session->set_userdata('notif', '<script type="text/javascript">
            swal("Berhasil!", "Tambah Sparepart Berhasil", "success");
            </script>');
            redirect('sparepart');
    }
    function update(){
    	$id = $this->input->post('id');
    	$merk = $this->input->post('merk');
        $jenis = $this->input->post('jenis');
        $sparepart = $this->input->post('sparepart');
        $harga = $this->input->post('harga');
        $garansi = $this->input->post('garansi');

        $data = array(
                        'merk' => $merk,
                        'jenis' => $jenis,
                        'sparepart' => $sparepart,
                        'harga' => $harga,
                        'garansi' => $garansi
                     );
        $this->db->update('tbl_sparepart', $data, array('id' => $id));
        $this->session->set_userdata('notif', '<script type="text/javascript">
        swal("Berhasil!", "Edit Sparepart Berhasil", "success");
        </script>');
        redirect('sparepart');
    }
    function delete($id){
    	$this->db->delete('tbl_sparepart', array('id' => $id));
    	$this->session->set_userdata('notif', '<script type="text/javascript">
        	swal("Berhasil!", "Hapus sparepart Berhasil", "success");
      		</script>');
        redirect('kategori');
    }
}
?>