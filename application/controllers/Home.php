<?php
class Home extends CI_Controller
{
	function index(){
			$data['title'] = "Toko Sparepart";
			// $data['sparepart'] = $this->db->get('tbl_sparepart')->result();
			// $data['isi'] = $this->load->view('front/content',[],true);
			$this->load->view('front/index',$data);
		}
	function proses(){
		$this->load->model('Login_m');
		$this->db->truncate('tbl_pencarian');
		$this->db->truncate('tbl_alternatif');
		$this->db->truncate('tbl_normalisasi');
		$this->db->truncate('tbl_ranking');
		$merk = $this->input->post('merk');
		$jenis = $this->input->post('jenis');
		$sparepart = $this->input->post('sparepart');

		$data = array(
						'merk' => $merk,
						'jenis' => $jenis,
						'sparepart' => $sparepart,
						'is_finish' => 0
					 );
		$this->db->insert('tbl_pencarian', $data);
		$get_all_data = $this->db->get('tbl_sparepart')->result();
		$C2 = '';
		$C3 = '';
		$C4 = '';
		$C5 = '';
 		$get_C4 = $this->db->get_where('tbl_crips', array('kode_kriteria' => 'C4'))->result_array();
 		$get_C4_1 = str_replace("C4 <= ", "", $get_C4[0]['crips']);
 		$get_C4_2 = str_replace("25000 < C4 <= ", "", $get_C4[1]['crips']);
 		$get_C4_3 = str_replace("50000 < C4 <= ", "", $get_C4[2]['crips']);
 		$get_C4_4 = str_replace("100000 < C4 <= ", "", $get_C4[3]['crips']);
 		$get_C4_5 = str_replace("C4 > ", "", $get_C4[4]['crips']);

 		$get_C5 = $this->db->get_where('tbl_crips', array('kode_kriteria' => 'C5'))->result_array();
 		$get_C5_1 = str_replace("C5 <= ", "", $get_C5[0]['crips']);
 		$get_C5_2 = str_replace("C5 = ", "", $get_C5[1]['crips']);
 		$get_C5_3 = str_replace("C5 = ", "", $get_C5[2]['crips']);
 		$get_C5_4 = str_replace("C5 = ", "", $get_C5[3]['crips']);
 		$get_C5_5 = str_replace("C5 > ", "", $get_C5[4]['crips']);
		foreach ($get_all_data as $s) {
			# code...
			$C1 = $this->db->get_where('tbl_crips', array('nilai' => $s->merk))->row()->nilai;
			$get_C2 = $this->db->get_where('tbl_sparepart', array('merk' => $merk, 'jenis' => $jenis))->num_rows();
			$get_C3 = $this->db->query('SELECT * FROM tbl_sparepart WHERE merk = '.$merk.' AND jenis LIKE "%'.$jenis.'%" AND sparepart LIKE "%'.$sparepart.'%"')->num_rows();
			$cek_jenis = $this->Login_m->like($s->jenis,$jenis);
			$cek_sparepart = $this->Login_m->like($s->sparepart,$sparepart);
			if (($s->merk == $merk) && ($cek_jenis == true)) {
				# code...
				$C2 = 2;
			}else{ 
				$C2 = 1;
			}

			if ($cek_sparepart == true) {
				# code...
				$C3 = 2;
			}else{
				$C3 = 1;
			}


			if ($s->harga <= $get_C4_1) {
				# code...
				$C4 = $get_C4[0]['nilai'];
			}elseif (($get_C4_1 < $s->harga) && ($s->harga <= $get_C4_2)) {
				# code...
				$C4 = $get_C4[1]['nilai'];
			}elseif (($get_C4_2 < $s->harga) && ($s->harga <= $get_C4_3)) {
				# code...
				$C4 = $get_C4[2]['nilai'];
			}elseif (($get_C4_3 < $s->harga) && ($s->harga <= $get_C4_4)) {
				# code...
				$C4 = $get_C4[3]['nilai'];
			}elseif ($s->harga > $get_C4_5) {
				# code...
				$C4 = $get_C4[4]['nilai'];
			}

			if ($s->garansi <= $get_C5_1) {
				# code...
				$C5 = $get_C5[0]['nilai'];
			}elseif ($s->garansi == $get_C5_2) {
				# code...
				$C5 = $get_C5[1]['nilai'];
			}elseif ($s->garansi == $get_C5_3) {
				# code...
				$C5 = $get_C5[2]['nilai'];
			}elseif ($s->garansi == $get_C5_4) {
				# code...
				$C5 = $get_C5[3]['nilai'];
			}elseif ($s->garansi > $get_C5_5) {
				# code...
				$C5 = $get_C5[4]['nilai'];
			}

			$data_alt = array(
								'kode_alt' => 'A-'.$s->id,
								'C1' => $C1,
								'C2' => $C2,
								'C3' => $C3,
								'C4' => $C4,
								'C5' => $C5,
							 );
			$this->db->insert('tbl_alternatif', $data_alt);
		}

		$get_atribute_C1 = $this->db->get_where('tbl_kriteria', array('kode_kriteria' => 'C1'))->row();
		$get_atribute_C2 = $this->db->get_where('tbl_kriteria', array('kode_kriteria' => 'C2'))->row();
		$get_atribute_C3 = $this->db->get_where('tbl_kriteria', array('kode_kriteria' => 'C3'))->row();
		$get_atribute_C4 = $this->db->get_where('tbl_kriteria', array('kode_kriteria' => 'C4'))->row();
		$get_atribute_C5 = $this->db->get_where('tbl_kriteria', array('kode_kriteria' => 'C5'))->row();
		$C1_normal = '';
		$C2_normal = '';
		$C3_normal = '';
		$C4_normal = '';
		$C5_normal = '';

		$get_alt = $this->db->get('tbl_alternatif')->result();
		foreach ($get_alt as $alt) {
			# code...
			if ($get_atribute_C1->atribut == 1) {
				# code...
				$get_min_c1 = $this->db->query('SELECT MIN(C1) as C1 FROM tbl_alternatif')->row()->C1;
				$C1_normal = $alt->C1 / $get_min_c1;
			}elseif ($get_atribute_C1->atribut == 2) {
				# code...
				$get_max_c1 = $this->db->query('SELECT MAX(C1) as C1 FROM tbl_alternatif')->row()->C1;
				$C1_normal = $alt->C1 / $get_max_c1;
			}
			if ($get_atribute_C2->atribut == 1) {
				# code...
				$get_min_c2 = $this->db->query('SELECT MIN(C2) as C2 FROM tbl_alternatif')->row()->C2;
				$C2_normal = $alt->C2 / $get_min_c2;
			}elseif ($get_atribute_C2->atribut == 2) {
				# code...
				$get_max_c2 = $this->db->query('SELECT MAX(C2) as C2 FROM tbl_alternatif')->row()->C2;
				$C2_normal = $alt->C2 / $get_max_c2;
			}
			if ($get_atribute_C3->atribut == 1) {
				# code...
				$get_min_c3 = $this->db->query('SELECT MIN(C3) as C3 FROM tbl_alternatif')->row()->C3;
				$C3_normal = $alt->C3 / $get_min_c3;
			}elseif ($get_atribute_C3->atribut == 2) {
				# code...
				$get_max_c3 = $this->db->query('SELECT MAX(C3) as C3 FROM tbl_alternatif')->row()->C3;
				$C3_normal = $alt->C3 / $get_max_c3;
			}
			if ($get_atribute_C4->atribut == 1) {
				# code...
				$get_min_c4 = $this->db->query('SELECT MIN(C4) as C4 FROM tbl_alternatif')->row()->C4;
				$C4_normal = $alt->C4 / $get_min_c4;
			}elseif ($get_atribute_C4->atribut == 2) {
				# code...
				$get_max_c4 = $this->db->query('SELECT MAX(C4) as C4 FROM tbl_alternatif')->row()->C4;
				$C4_normal = $alt->C4 / $get_max_c4;
			}
			if ($get_atribute_C5->atribut == 1) {
				# code...
				$get_min_c5 = $this->db->query('SELECT MIN(C5) as C5 FROM tbl_alternatif')->row()->C5;
				$C5_normal = $alt->C5 / $get_min_c5;
			}elseif ($get_atribute_C5->atribut == 2) {
				# code...
				$get_max_c5 = $this->db->query('SELECT MAX(C5) as C5 FROM tbl_alternatif')->row()->C5;
				$C5_normal = $alt->C5 / $get_max_c5;
			}
			$data_normalisasi = array(
										'kode_alt' => $alt->kode_alt,
										'C1' => $C1_normal,
										'C2' => $C2_normal,
										'C3' => $C3_normal,
										'C4' => $C4_normal,
										'C5' => $C5_normal
							   		  );
			$this->db->insert('tbl_normalisasi', $data_normalisasi);
		}

		$get_bobot = $this->db->get('tbl_kriteria')->result_array();
		$get_data_normalisasi = $this->db->get('tbl_normalisasi')->result();
		$total_alt = 0;
		foreach ($get_data_normalisasi as $nor) {
			# code...
			$total_alt = ($nor->C1 * $get_bobot[0]['bobot']) + ($nor->C2 * $get_bobot[1]['bobot']) + ($nor->C3 * $get_bobot[2]['bobot']) + ($nor->C3 * $get_bobot[3]['bobot']) + ($nor->C4 * $get_bobot[4]['bobot']);
			$data_rangking = array(
									'kode_alt' => $nor->kode_alt,
									'total_alt' => $total_alt
								  );
			$this->db->insert('tbl_ranking', $data_rangking);
		}
		$get_ranking = $this->db->query('SELECT * FROM `tbl_ranking` ORDER BY total_alt DESC LIMIT 1')->row();
		$data_session = array(
								'kode_alt' => $get_ranking->kode_alt,
								'ranking' => $get_ranking->total_alt
							 );
		$this->session->set_userdata($data_session);
		redirect(base_url('home'));
	}
	function log(){
		$data['title'] = "Toko Sparepart";
		$data['sparepart'] = $this->db->get('tbl_sparepart')->result();
		$data['alternatif'] = $this->db->get('tbl_alternatif')->result();
		$data['normalisasi'] = $this->db->get('tbl_normalisasi')->result();
		$data['rangking'] = $this->db->get('tbl_ranking')->result();
		// $data['isi'] = $this->load->view('front/content',[],true);
		$this->load->view('front/content',$data);
	}
}
?>