<?php
	class Kriteria extends CI_Controller
	{
		
		function __construct()
		{
				parent::__construct();
				if($this->session->userdata('login') != 'login_admin')
				{
					redirect(base_url());
				}
				$this->load->model('Login_m');
		}
		function index(){
			$data['title'] = "SPK dengan SAW";
	        $data['menu'] = $this->Login_m->menu();
	        // $id_user = $this->db->get_where('tbl_user', array('username' => $this->session->userdata('username')))->row()->id;
	        $content['list'] = $this->db->get('tbl_kriteria')->result();
	        $content['jml'] = $this->db->get('tbl_kriteria')->num_rows();
			$data['content'] = $this->load->view('dashboard/list_kriteria',$content,true);
			$this->load->view('dashboard/index',$data);
		}
		function tambah(){
			$kode_kriteria = $this->input->post('kode');
			$nama_kriteria = $this->input->post('nama');
			$atribut = $this->input->post('atribut');
			$bobot = $this->input->post('bobot');

			$cek = $this->db->get_where('tbl_kriteria', array('kode_kriteria' => $kode_kriteria))->num_rows();
			if ($cek > 0) {
				# code...
				$this->session->set_userdata('notif', '<script type="text/javascript">
	        	swal("Gagal!", "Kriteria sudah tersedia", "error");
	      		</script>');
	            redirect('kriteria');
			}else{
				$jml_bobot = $this->db->query('SELECT SUM(bobot) as bbt FROM tbl_kriteria')->row()->bbt;
				$bbt = 0;
				$bbt = $jml_bobot + $bobot;
				if ($bbt > 100) {
					# code...
					$this->session->set_userdata('notif', '<script type="text/javascript">
		        	swal("Gagal!", "Bobot Maksimal 100", "error");
		      		</script>');
		            redirect('kriteria');
				}else{
					$data = array(
								'kode_kriteria' => $kode_kriteria,
								'nama_kriteria' => $nama_kriteria,
								'atribut' => $atribut,
								'bobot' => $bobot
							 );
					$this->db->insert('tbl_kriteria', $data);
					$this->session->set_userdata('notif', '<script type="text/javascript">
		        	swal("Berhasil!", "Kriteria berhasil ditambahkan", "success");
		      		</script>');
		            redirect('kriteria');
		        }
			}
		}
		function update(){
			$kode_kriteria = $this->input->post('kode');
			$nama_kriteria = $this->input->post('nama');
			$atribut = $this->input->post('atribut');
			$bobot = $this->input->post('bobot');
			$cek = $this->db->get_where('tbl_crips', array('kode_kriteria' => $kode_kriteria))->num_rows();
			for ($i=1; $i <= 5; $i++) { 
				# code...
				$crips = $this->input->post('crips'.$i);
				$nilai = $this->input->post('nilai'.$i);
				if (!empty($crips) && !empty($nilai)) {
					# code...
					$data_crips[] = array(
										'kode_kriteria' => $kode_kriteria,
										'nama_kriteria' => $nama_kriteria,
										'crips' => $crips,
										'nilai' => $nilai
									);
				}
			}
			$jml = count($data_crips);
			var_dump($data_crips);
			if ($cek == $jml) {
				# code...
				$this->db->update_batch('tbl_crips', $data_crips, 'crips');
			}else{
				$this->db->insert_batch('tbl_crips', $data_crips);
			}
			$data = array(
							'nama_kriteria' => $nama_kriteria,
							'atribut' => $atribut,
							'bobot' => $bobot
						 );
			$this->db->update('tbl_kriteria', $data, array('kode_kriteria' => $kode_kriteria));
			$this->session->set_userdata('notif', '<script type="text/javascript">
	        	swal("Berhasil!", "Kriteria berhasil diubah", "success");
	      		</script>');
	            redirect('kriteria');
		}
		function delete($id){
			$this->db->delete('tbl_kriteria', array('id' => $id));
			$this->session->set_userdata('notif', '<script type="text/javascript">
	        	swal("Berhasil!", "Kriteria berhasil dihapus", "success");
	      		</script>');
	            redirect('kriteria');
		}
		function export()
		{
		    // Load plugin PHPExcel nya
		    include APPPATH.'third_party/PHPExcel/PHPExcel.php';
		    
		    // Panggil class PHPExcel nya
		    $excel = new PHPExcel();
		    // Settingan awal fil excel
		    $excel->getProperties()->setCreator('Daftar Pembayaran Lunas')
		                 ->setLastModifiedBy('Pembayaran Lunas')
		                 ->setTitle("Pembayaran Lunas")
		                 ->setSubject("Lunas")
		                 ->setDescription("Laporan Semua Data Lunas")
		                 ->setKeywords("Data Lunas");
		    // Buat sebuah variabel untuk menampung pengaturan style dari header tabel
		    $style_col = array(
		      'font' => array('bold' => true), // Set font nya jadi bold
		      'alignment' => array(
		        'horizontal' => PHPExcel_Style_Alignment::HORIZONTAL_CENTER, // Set text jadi ditengah secara horizontal (center)
		        'vertical' => PHPExcel_Style_Alignment::VERTICAL_CENTER // Set text jadi di tengah secara vertical (middle)
		      ),
		      'borders' => array(
		        'top' => array('style'  => PHPExcel_Style_Border::BORDER_THIN), // Set border top dengan garis tipis
		        'right' => array('style'  => PHPExcel_Style_Border::BORDER_THIN),  // Set border right dengan garis tipis
		        'bottom' => array('style'  => PHPExcel_Style_Border::BORDER_THIN), // Set border bottom dengan garis tipis
		        'left' => array('style'  => PHPExcel_Style_Border::BORDER_THIN) // Set border left dengan garis tipis
		      )
		    );
		    // Buat sebuah variabel untuk menampung pengaturan style dari isi tabel
		    $style_row = array(
		      'alignment' => array(
		        'vertical' => PHPExcel_Style_Alignment::VERTICAL_CENTER // Set text jadi di tengah secara vertical (middle)
		      ),
		      'borders' => array(
		        'top' => array('style'  => PHPExcel_Style_Border::BORDER_THIN), // Set border top dengan garis tipis
		        'right' => array('style'  => PHPExcel_Style_Border::BORDER_THIN),  // Set border right dengan garis tipis
		        'bottom' => array('style'  => PHPExcel_Style_Border::BORDER_THIN), // Set border bottom dengan garis tipis
		        'left' => array('style'  => PHPExcel_Style_Border::BORDER_THIN) // Set border left dengan garis tipis
		      )
		    );
		    $excel->setActiveSheetIndex(0)->setCellValue('A1', "DATA PEMBAYARAN LUNAS"); // Set kolom A1 dengan tulisan "DATA SISWA"
		    $excel->getActiveSheet()->mergeCells('A1:H1'); // Set Merge Cell pada kolom A1 sampai E1
		    $excel->getActiveSheet()->getStyle('A1')->getFont()->setBold(TRUE); // Set bold kolom A1
		    $excel->getActiveSheet()->getStyle('A1')->getFont()->setSize(15); // Set font size 15 untuk kolom A1
		    $excel->getActiveSheet()->getStyle('A1')->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_CENTER); // Set text center untuk kolom A1
		    // Buat header tabel nya pada baris ke 3
		    $excel->setActiveSheetIndex(0)->setCellValue('A3', "NO"); // Set kolom A3 dengan tulisan "NO"
		    $excel->setActiveSheetIndex(0)->setCellValue('B3', "Nama Pembeli"); // Set kolom B3 dengan tulisan "NIS"
		    $excel->setActiveSheetIndex(0)->setCellValue('C3', "Nomor HP"); // Set kolom C3 dengan tulisan "NAMA"
		    $excel->setActiveSheetIndex(0)->setCellValue('D3', "Alamat"); // Set kolom D3 dengan tulisan "JENIS KELAMIN"
		    $excel->setActiveSheetIndex(0)->setCellValue('E3', "Status Pembayaran"); // Set kolom E3 dengan tulisan "ALAMAT"
		    $excel->setActiveSheetIndex(0)->setCellValue('F3', "Terbayar"); // Set kolom E3 dengan tulisan "ALAMAT"
		    $excel->setActiveSheetIndex(0)->setCellValue('G3', "Total Tagihan"); // Set kolom E3 dengan tulisan "ALAMAT"
		    $excel->setActiveSheetIndex(0)->setCellValue('H3', "Tanggal Pembelian"); // Set kolom E3 dengan tulisan "ALAMAT"
		    // Apply style header yang telah kita buat tadi ke masing-masing kolom header
		    $excel->getActiveSheet()->getStyle('A3')->applyFromArray($style_col);
		    $excel->getActiveSheet()->getStyle('B3')->applyFromArray($style_col);
		    $excel->getActiveSheet()->getStyle('C3')->applyFromArray($style_col);
		    $excel->getActiveSheet()->getStyle('D3')->applyFromArray($style_col);
		    $excel->getActiveSheet()->getStyle('E3')->applyFromArray($style_col);
		    $excel->getActiveSheet()->getStyle('F3')->applyFromArray($style_col);
		    $excel->getActiveSheet()->getStyle('G3')->applyFromArray($style_col);
		    $excel->getActiveSheet()->getStyle('H3')->applyFromArray($style_col);
		    
		    $nasabah = $this->db->get_where('detail_cart', array('status_pembayaran' => 1))->result();
		    $no = 1; 
		    $numrow = 4; // Set baris pertama untuk isi tabel adalah baris ke 4
		    foreach($nasabah as $data){ 
		      $excel->setActiveSheetIndex(0)->setCellValue('A'.$numrow, $no);
		      $excel->setActiveSheetIndex(0)->setCellValue('B'.$numrow, $data->nama_pembeli);
		      $excel->setActiveSheetIndex(0)->setCellValue('C'.$numrow, $data->hp);
		      $excel->setActiveSheetIndex(0)->setCellValue('D'.$numrow, $data->alamat);
		      $excel->setActiveSheetIndex(0)->setCellValue('E'.$numrow, 'LUNAS');
		      $excel->setActiveSheetIndex(0)->setCellValue('F'.$numrow, $data->terbayar);
		      $excel->setActiveSheetIndex(0)->setCellValue('G'.$numrow, $data->total);
		      $excel->setActiveSheetIndex(0)->setCellValue('H'.$numrow, $data->tgl_pembelian);
		      
		      // Apply style row yang telah kita buat tadi ke masing-masing baris (isi tabel)
		      $excel->getActiveSheet()->getStyle('A'.$numrow)->applyFromArray($style_row);
		      $excel->getActiveSheet()->getStyle('B'.$numrow)->applyFromArray($style_row);
		      $excel->getActiveSheet()->getStyle('C'.$numrow)->applyFromArray($style_row);
		      $excel->getActiveSheet()->getStyle('D'.$numrow)->applyFromArray($style_row);
		      $excel->getActiveSheet()->getStyle('E'.$numrow)->applyFromArray($style_row);
		      $excel->getActiveSheet()->getStyle('F'.$numrow)->applyFromArray($style_row);
		      $excel->getActiveSheet()->getStyle('G'.$numrow)->applyFromArray($style_row);
		      $excel->getActiveSheet()->getStyle('H'.$numrow)->applyFromArray($style_row);
		      
		      $no++; // Tambah 1 setiap kali looping
		      $numrow++; // Tambah 1 setiap kali looping
		    }
		    // Set width kolom
		    $excel->getActiveSheet()->getColumnDimension('A')->setWidth(5); // Set width kolom A
		    $excel->getActiveSheet()->getColumnDimension('B')->setWidth(55); // Set width kolom B
		    $excel->getActiveSheet()->getColumnDimension('C')->setWidth(15); // Set width kolom C
		    $excel->getActiveSheet()->getColumnDimension('D')->setWidth(55); // Set width kolom D
		    $excel->getActiveSheet()->getColumnDimension('E')->setWidth(25); // Set width kolom E
		    $excel->getActiveSheet()->getColumnDimension('F')->setWidth(25); // Set width kolom E
		    $excel->getActiveSheet()->getColumnDimension('G')->setWidth(25); // Set width kolom E
		    $excel->getActiveSheet()->getColumnDimension('H')->setWidth(25); // Set width kolom E
		    
		    // Set height semua kolom menjadi auto (mengikuti height isi dari kolommnya, jadi otomatis)
		    $excel->getActiveSheet()->getDefaultRowDimension()->setRowHeight(-1);
		    // Set orientasi kertas jadi LANDSCAPE
		    $excel->getActiveSheet()->getPageSetup()->setOrientation(PHPExcel_Worksheet_PageSetup::ORIENTATION_LANDSCAPE);
		    // Set judul file excel nya
		    $excel->getActiveSheet(0)->setTitle("Data Pembayaran Lunas");
		    $excel->setActiveSheetIndex(0);
		    // Proses file excel
		    header('Content-Type: application/vnd.openxmlformats-officedocument.spreadsheetml.sheet');
		    header('Content-Disposition: attachment; filename="Data Pembayaran Lunas.xlsx"'); // Set nama file excel nya
		    header('Cache-Control: max-age=0');
		    $write = PHPExcel_IOFactory::createWriter($excel, 'Excel2007');
		    $write->save('php://output');
		  }
	}
?>