<?php 
/**
* 
*/
class Dashboard extends CI_Controller
{
	
	function __construct()
	{
				parent::__construct();
				if($this->session->userdata('login') != 'login_admin')
				{
					redirect(base_url());
				}
				$this->load->model('Login_m');
	}
	function index(){
		$data['title'] = "SPK dengan SAW";
		// $data['content'] = $this->db->get('tbl_website')->row();
		// $data['kategori'] = $this->db->get('tbl_kategori')->result();
		// $q = $this->db->get_where('tbl_customers', array('nomor_hp' => $this->session->userdata('hp')))->row('id');
		// $data['cart'] = $this->db->get_where('tbl_cart', array('id_customer' => $q))->num_rows();
		$data['menu'] = $this->Login_m->menu();
		$data['content'] = $this->load->view('dashboard/content',[],true);
		$this->load->view('dashboard/index',$data);
	}
}
?>