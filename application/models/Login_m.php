<?php  
/**
* 
*/
class Login_m extends CI_Model
{
	public function menu()
	{
		# code...
		$q = $this->db->get_where('tbl_menu', array('user_level' => $this->session->userdata('level'), 'parent' => 0, 'set_active' => 1));
		return $q->result();
	}
	public function subMenu($parent=''){
		$q = $this->db->get_where('tbl_menu', array('user_level' => $this->session->userdata('level'), 'parent' => $parent, 'set_active' => 1));
		return $q->result();
	}
	public function sMenu($id=''){
		$q = $this->db->get_where('tbl_menu', array('parent' => $id));
		return $q->num_rows();
	}
	function like($str, $searchTerm) {
    $searchTerm = strtolower($searchTerm);
    $str = strtolower($str);
    $pos = strpos($str, $searchTerm);
    if ($pos === false)
        return false;
    else
        return true;
	}
}
?>