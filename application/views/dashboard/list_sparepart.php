<div class="container-fluid">
        <?php 
          echo $this->session->userdata('notif'); 
          $this->session->set_userdata('notif',''); 
          function rupiah($angka){
	
            $hasil_rupiah = "Rp " . number_format($angka,2,',','.');
            return $hasil_rupiah;
         
        }
        ?>
          <!-- Page Heading -->
          <h1 class="h3 mb-2 text-gray-800">Daftar Sparepart</h1>

          <!-- DataTales Example -->
          <div class="card shadow mb-4">
            <div class="card-header py-3">
              <h6 class="m-0 font-weight-bold text-primary">Daftar Sparepart</h6>
            </div>
            <div class="card-body">
                <a href="#" data-toggle="modal" data-target="#myModal" class="btn btn-primary btn-icon-split">
                            <span class="icon text-white-50">
                              <i class="fas fa-edit"></i>
                            </span>
                            <span class="text">Tambahkan Sparepart</span>
                          </a> 
                <!-- <a href="<?php echo base_url('produk/export'); ?>" class="btn btn-success btn-icon-split">
                            <span class="icon text-white-50">
                              <i class="fas fa-edit"></i>
                            </span>
                            <span class="text">Download Data</span>
                          </a>  -->
              <hr>
              <!-- Modal -->
                <div class="modal fade" id="myModal" role="dialog">
                    <div class="modal-dialog">
                    
                    <!-- Modal content-->
                    <div class="modal-content">
                        <div class="modal-header">
                        <button type="button" class="close" data-dismiss="modal">&times;</button>
                        </div>
                        <div class="modal-body">
                        <center><p><b>Tambah Sparepart</b></p></center>
                        <!-- <br> -->
                        <form action="<?php echo base_url('sparepart/tambah/');?>" method="post" enctype="multipart/form-data">
                        <table width="100%">
                            <tr>
                                <td style="padding:5px;">Merk Motor</td>
                                <td style="padding:5px;"> : </td>
                                <td style="padding:5px;">
                                  <select name="merk" class="form-control">
                                    <option selected="" disabled="">-- Merk Motor --</option>
                                    <option value="5">Honda</option>
                                    <option value="4">Yamaha</option>
                                    <option value="3">Kawasaki</option>
                                    <option value="2">Suzuki</option>
                                    <option value="1">Lainnya</option>
                                  </select>
                                </td>
                            </tr>
                            <tr>
                                <td style="padding:5px;">Jenis Motor</td>
                                <td style="padding:5px;"> : </td>
                                <td style="padding:5px;">
                                  <input type="text" name="jenis" class="form-control" placeholder="Jenis Motor">
                                </td>
                            </tr>
                            <tr>
                                <td style="padding:5px;">Nama Sparepart</td>
                                <td style="padding:5px;"> : </td>
                                <td style="padding:5px;">
                                  <input type="text" name="sparepart" class="form-control" placeholder="Sparepart Motor">
                                </td>
                            </tr>
                            <tr>
                                <td style="padding:5px;">Harga Sparepart</td>
                                <td style="padding:5px;"> : </td>
                                <td style="padding:5px;">
                                  <input type="number" name="harga" class="form-control" placeholder="Harga Sparepart">
                                </td>
                            </tr>
                            <tr>
                                <td style="padding:5px;">Garansi Sparepart</td>
                                <td style="padding:5px;"> : </td>
                                <td style="padding:5px;">
                                  <input type="number" name="garansi" class="form-control" placeholder="Garansi Sparepart(Bulan)">
                                </td>
                            </tr>
                            <tr>
                                <td colspan="3"  style="padding:5px;"><center><input class="btn btn-primary" type="submit" value="Tambah"/></center></td>
                            </tr>
                        </table>
                        </form>
                        </div>
                        <div class="modal-footer">
                        <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                        </div>
                    </div>
                    
                    </div>
                </div>
              <div class="table-responsive">
                <table class="table table-bordered" id="dataTable" width="100%" cellspacing="0">
                <thead>
                  <tr>
                    <th>No.</th>
                    <th>Merk Motor</th>
                    <th>Jenis Motor</th>
                    <th>Sparepart Motor</th>
                    <th>Harga</th>
                    <th>Garansi</th>
                    <th></th>
                  </tr>
                </thead>
                <tbody>
                    <?php $t = 1; $m= 1; $no=1; foreach ($kategori as $p) { ?>
                    <tr>
                        <td><?php echo $no++ ?></td>
                        <td><?php if($p->merk == 5){ echo "Honda"; }elseif($p->merk == 4){echo "Yamaha";}elseif($p->merk == 3){echo "Kawasaki";}elseif($p->merk == 2){echo "Suzuki";}else{echo "Lainnya";} ?></td>
                        <td><?php echo $p->jenis ?></td>
                        <td><?php echo $p->sparepart ?></td>
                        <td><?php echo rupiah($p->harga); ?></td>
                        <td><?php echo $p->garansi." / Bulan"; ?></td>
                        <td><a href="#" title="Edit" data-toggle="modal" data-target="#modal_edit<?php echo $t++ ?>" class="btn btn-warning btn-icon-split">
                            <span class="icon text-white-50">
                              <i class="fas fa-edit"></i>
                            </span>
                          </a>
                          <!-- Modal -->
                            <div class="modal fade" id="modal_edit<?php echo $m++ ?>" role="dialog">
                                <div class="modal-dialog">
                                
                                <!-- Modal content-->
                                <div class="modal-content">
                                    <div class="modal-header">
                                    <button type="button" class="close" data-dismiss="modal">&times;</button>
                                    </div>
                                    <div class="modal-body">
                                    <center><p><b>Edit Sparepart</b></p></center>
                                    <!-- <br> -->
                                    <form action="<?php echo base_url('sparepart/update/');?>" method="post" enctype="multipart/form-data">
                                     <table width="100%">
                                        <tr>
                                            <td style="padding:5px;"><input type="text" name="id" class="form-control" value="<?php echo $p->id ?>" hidden="">Merk Motor</td>
                                            <td style="padding:5px;"> : </td>
                                            <td style="padding:5px;">
                                              <select name="merk" class="form-control">
                                                <option selected="" value="<?php echo $p->merk ?>"><?php if($p->merk == 5){ echo "Honda"; }elseif($p->merk == 4){echo "Yamaha";}elseif($p->merk == 3){echo "Kawasaki";}elseif($p->merk == 2){echo "Suzuki";}else{echo "Lainnya";} ?></option>
                                                <option value="5">Honda</option>
                                                <option value="4">Yamaha</option>
                                                <option value="3">Kawasaki</option>
                                                <option value="2">Suzuki</option>
                                                <option value="1">Lainnya</option>
                                              </select>
                                            </td>
                                        </tr>
                                        <tr>
                                            <td style="padding:5px;">Jenis Motor</td>
                                            <td style="padding:5px;"> : </td>
                                            <td style="padding:5px;">
                                              <input type="text" name="jenis" class="form-control" value="<?php echo $p->jenis ?>">
                                            </td>
                                        </tr>
                                        <tr>
                                            <td style="padding:5px;">Nama Sparepart</td>
                                            <td style="padding:5px;"> : </td>
                                            <td style="padding:5px;">
                                              <input type="text" name="sparepart" class="form-control" value="<?php echo $p->sparepart ?>">
                                            </td>
                                        </tr>
                                        <tr>
                                            <td style="padding:5px;">Harga Sparepart</td>
                                            <td style="padding:5px;"> : </td>
                                            <td style="padding:5px;">
                                              <input type="number" name="harga" class="form-control" value="<?php echo $p->harga ?>">
                                            </td>
                                        </tr>
                                        <tr>
                                            <td style="padding:5px;">Garansi Sparepart</td>
                                            <td style="padding:5px;"> : </td>
                                            <td style="padding:5px;">
                                              <input type="number" name="garansi" class="form-control" value="<?php echo $p->garansi ?>">
                                            </td>
                                        </tr>
                                        <tr>
                                            <td colspan="3"  style="padding:5px;"><center><input class="btn btn-primary" type="submit" value="Edit"/></center></td>
                                        </tr>
                                    </table>
                                    </form>
                                    </div>
                                    <div class="modal-footer">
                                    <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                                    </div>
                                </div>
                                
                                </div>
                            </div>
                          &nbsp;<a onclick="return confirm('Yakin ingin menghapus sparepart?')" href="<?php echo base_url('sparepart/delete/'.$p->id);?>" title="Hapus" class="btn btn-danger btn-icon-split">
                            <span class="icon text-white-50">
                              <i class="fas fa-trash"></i>
                            </span>
                          </a> </td>
                    </tr>
                    <?php } ?>
                </tbody>
                </table>
              </div>
            </div>
          </div>

        </div>
        <!-- /.container-fluid
