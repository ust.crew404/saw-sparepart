<div class="container-fluid">
        <?php 
          echo $this->session->userdata('notif'); 
          $this->session->set_userdata('notif',''); 
          function rupiah($angka){
	
            $hasil_rupiah = "Rp " . number_format($angka,2,',','.');
            return $hasil_rupiah;
         
        }
        ?>
          <!-- Page Heading -->
          <h1 class="h3 mb-2 text-gray-800">Daftar Kriteria</h1>

          <!-- DataTales Example -->
          <div class="card shadow mb-4">
            <div class="card-header py-3">
              <h6 class="m-0 font-weight-bold text-primary">Data Kriteria</h6>
            </div>
            <div class="card-body">
                <?php if($jml < 5){?>
                <a href="#" data-toggle="modal" data-target="#myModal" class="btn btn-primary btn-icon-split">
                            <span class="icon text-white-50">
                              <i class="fas fa-edit"></i>
                            </span>
                            <span class="text">Tambahkan Kriteria</span>
                          </a> 
                <?php } ?>
                <!-- <a href="<?php echo base_url('produk/export'); ?>" class="btn btn-success btn-icon-split">
                            <span class="icon text-white-50">
                              <i class="fas fa-edit"></i>
                            </span>
                            <span class="text">Download Data</span>
                          </a>  -->
              <hr>
              <!-- Modal -->
                <div class="modal fade" id="myModal" role="dialog">
                    <div class="modal-dialog">
                    
                    <!-- Modal content-->
                    <div class="modal-content">
                        <div class="modal-header">
                        <button type="button" class="close" data-dismiss="modal">&times;</button>
                        </div>
                        <div class="modal-body">
                        <center><p><b>Tambah Kriteria</b></p></center>
                        <!-- <br> -->
                        <form action="<?php echo base_url('kriteria/tambah/');?>" method="post" enctype="multipart/form-data">
                        <table width="100%">
                            <tr>
                                <td style="padding:5px;">Kode Kriteria</td>
                                <td style="padding:5px;"> : </td>
                                <td style="padding:5px;"><input class="form-control" type="text" name="kode" placeholder="Kode Kriteria" /></td>
                            </tr>
                            <tr>
                                <td style="padding:5px;">Nama Kriteria</td>
                                <td style="padding:5px;"> : </td>
                                <td style="padding:5px;"><input class="form-control" placeholder="Nama Kriteria" type="text" name="nama" /></td>
                            </tr>
                            <tr>
                                <td style="padding:5px;">Atribut</td>
                                <td style="padding:5px;"> : </td>
                                <td style="padding:5px;">
                                    <select name="atribut" class="form-control">
                                        <option selected="" disabled="">-- Pilih Kriteria --</option>
                                        <option value="1">Cost</option>
                                        <option value="2">Benefit</option>
                                    </select>
                                </td>
                            </tr>
                            <tr>
                                <td style="padding:5px;">Bobot</td>
                                <td style="padding:5px;"> : </td>
                                <td style="padding:5px;"><input class="form-control" type="number" name="bobot" min="0" value="0" /></td>
                            </tr>
                            <tr>
                                <td colspan="3"  style="padding:5px;"><center><input class="btn btn-primary" type="submit" value="Tambah"/></center></td>
                            </tr>
                        </table>
                        </form>
                        </div>
                        <div class="modal-footer">
                        <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                        </div>
                    </div>
                    
                    </div>
                </div>
              <div class="table-responsive">
                <table class="table table-bordered" id="dataTable" width="100%" cellspacing="0">
                <thead>
                  <tr>
                    <th>No.</th>
                    <th>Kode Kriteria</th>
                    <th>Nama Kriteria</th>
                    <th>Atribut</th>
                    <th>Bobot</th>
                    <th></th>
                  </tr>
                </thead>
                <!-- <tfoot>
                  <tr>
                  <th>No.</th>
                    <th>Kode Barang</th>
                    <th>Nama Barang</th>
                    <th>Kategori</th>
                    <th>Jumlah Barang</th>
                    <th>Harga Barang</th>
                    <th>Gambar Barang</th>
                    <th></th>
                  </tr>
                </tfoot> -->
                <tbody>
                    <?php $t = 1; $m= 1; $no=1; foreach ($list as $p) { ?>
                    <tr>
                        <td><?php echo $no++ ?></td>
                        <td><?php echo $p->kode_kriteria ?></td>
                        <td><?php echo $p->nama_kriteria ?></td>
                        <td><?php if($p->atribut == 1){ echo "Cost"; }else{ echo "Benefit"; } ?></td>
                        <td><?php echo $p->bobot ?></td>
                        <td><a href="#" title="Edit" data-toggle="modal" data-target="#modal_edit<?php echo $t++ ?>" class="btn btn-warning btn-icon-split">
                            <span class="icon text-white-50">
                              <i class="fas fa-edit"></i>
                            </span>
                          </a>
                          <!-- Modal -->
                            <div class="modal fade" id="modal_edit<?php echo $m++ ?>" role="dialog">
                                <div class="modal-dialog">
                                
                                <!-- Modal content-->
                                <div class="modal-content">
                                    <div class="modal-header">
                                    <button type="button" class="close" data-dismiss="modal">&times;</button>
                                    </div>
                                    <div class="modal-body">
                                    <center><p><b>Edit Kriteria</b></p></center>
                                    <!-- <br> -->
                                    <form action="<?php echo base_url('kriteria/update/');?>" method="post" enctype="multipart/form-data">
                                    <table width="100%" border="0">
                                        <tr>
                                            <td style="padding:5px;">Kode Kriteria</td>
                                            <td style="padding:5px;"> : </td>
                                            <td style="padding:5px;" colspan="2"><input class="form-control" type="text" name="kode" value="<?php echo $p->kode_kriteria ?>" readonly=""/></td>
                                        </tr>
                                        <tr>
                                            <td style="padding:5px;">Nama Barang</td>
                                            <td style="padding:5px;"> : </td>
                                            <td style="padding:5px;" colspan="2"><input class="form-control" value="<?php echo $p->nama_kriteria ?>" type="text" name="nama" /></td>
                                        </tr>
                                        <tr>
                                            <td style="padding:5px;">Atribut</td>
                                            <td style="padding:5px;"> : </td>
                                            <td style="padding:5px;" colspan="2">
                                            <select class="form-control" name="atribut">
                                                <option disabled="" selected="">-- Pilih Kriteria --</option>
                                                <option value="<?php echo $p->atribut ?>" selected=""><?php if($p->atribut == 1){ echo "Cost"; }else{ echo "Benefit"; } ?></option>
                                                <option value="1">Cost</option>
                                                <option value="2">Benefit</option>
                                            </select></td>
                                        </tr>
                                        <tr>
                                            <td style="padding:5px;">Bobot</td>
                                            <td style="padding:5px;"> : </td>
                                            <td style="padding:5px;" colspan="2"><input class="form-control" type="number" value="<?php echo $p->bobot ?>" name="bobot" min="0" value="0" step="any" /></td>
                                        </tr>
                                        <tr>
                                            <td style="padding: 10px">Kode Kriteria</td>
                                            <td style="padding: 10px">Nama Kriteria</td>
                                            <td style="padding: 10px">Crips</td>
                                            <td style="padding: 10px">Nilai</td>
                                        </tr>
                                        <?php
                                            $get = $this->db->get_where('tbl_crips', array('kode_kriteria' => $p->kode_kriteria));
                                            $c = 1;
                                            $n = 1;
                                            if ($get->num_rows() > 0) { 
                                                foreach($get->result() as $g){ ?>
                                                    <tr>
                                                        <td style="padding: 10px"><?php echo $p->kode_kriteria ?></td>
                                                        <td style="padding: 10px"><?php echo $p->nama_kriteria ?></td>
                                                        <td style="padding: 10px"><input type="text" name="crips<?php echo $c++ ?>" class="form-control" value="<?php echo $g->crips ?>"></td>
                                                        <td style="padding: 10px"><input type="text" name="nilai<?php echo $n++ ?>" class="form-control" value="<?php echo $g->nilai ?>"></td>
                                                    </tr>
                                        <?php  
                                                }     
                                            }else{
                                        ?>
                                        <tr>
                                            <td style="padding: 10px"><?php echo $p->kode_kriteria ?></td>
                                            <td style="padding: 10px"><?php echo $p->nama_kriteria ?></td>
                                            <td style="padding: 10px"><input type="text" name="crips1" class="form-control" placeholder="Input Crips"></td>
                                            <td style="padding: 10px"><input type="text" name="nilai1" class="form-control" placeholder="0" ></td>
                                        </tr>
                                        <tr>
                                            <td style="padding: 10px"><?php echo $p->kode_kriteria ?></td>
                                            <td style="padding: 10px"><?php echo $p->nama_kriteria ?></td>
                                            <td style="padding: 10px"><input type="text" name="crips2" class="form-control" placeholder="Input Crips"></td>
                                            <td style="padding: 10px"><input type="text" name="nilai2" class="form-control" placeholder="0" ></td>
                                        </tr>
                                        <tr>
                                            <td style="padding: 10px"><?php echo $p->kode_kriteria ?></td>
                                            <td style="padding: 10px"><?php echo $p->nama_kriteria ?></td>
                                            <td style="padding: 10px"><input type="text" name="crips3" class="form-control" placeholder="Input Crips"></td>
                                            <td style="padding: 10px"><input type="text" name="nilai3" class="form-control" placeholder="0" ></td>
                                        </tr>
                                        <tr>
                                            <td style="padding: 10px"><?php echo $p->kode_kriteria ?></td>
                                            <td style="padding: 10px"><?php echo $p->nama_kriteria ?></td>
                                            <td style="padding: 10px"><input type="text" name="crips4" class="form-control" placeholder="Input Crips"></td>
                                            <td style="padding: 10px"><input type="text" name="nilai4" class="form-control" placeholder="0" ></td>
                                        </tr>
                                        <tr>
                                            <td style="padding: 10px"><?php echo $p->kode_kriteria ?></td>
                                            <td style="padding: 10px"><?php echo $p->nama_kriteria ?></td>
                                            <td style="padding: 10px"><input type="text" name="crips5" class="form-control" placeholder="Input Crips"></td>
                                            <td style="padding: 10px"><input type="text" name="nilai5" class="form-control" placeholder="0" ></td>
                                        </tr>
                                        <?php } ?>
                                        <tr>
                                            <td colspan="5"  style="padding:5px;"><center><input class="btn btn-primary" type="submit" value="Edit"/></center></td>
                                        </tr>
                                    </table>
                                    </form>
                                    </div>
                                    <div class="modal-footer">
                                    <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                                    </div>
                                </div>
                                
                                </div>
                            </div>
                          &nbsp;<a onclick="return confirm('Yakin ingin menghapus kriteria?')" href="<?php echo base_url('kriteria/delete/'.$p->id);?>" title="Hapus" class="btn btn-danger btn-icon-split">
                            <span class="icon text-white-50">
                              <i class="fas fa-trash"></i>
                            </span>
                          </a> </td>
                    </tr>
                    <?php } ?>
                </tbody>
                </table>
              </div>
            </div>
          </div>

        </div>
        <!-- /.container-fluid
