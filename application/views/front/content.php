<?php 
          function rupiah($angka){
    
            $hasil_rupiah = number_format($angka,2,',','.');
            return $hasil_rupiah;
         
        }
?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Strict//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-strict.dtd">
<!--
Design by TEMPLATED
http://templated.co
Released for free under the Creative Commons Attribution License

Name       : PlainDisplay 
Description: A two-column, fixed-width design with dark color scheme.
Version    : 1.0
Released   : 20140309

-->
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<title></title>
<meta name="keywords" content="" />
<meta name="description" content="" />
<link href="http://fonts.googleapis.com/css?family=Varela" rel="stylesheet" />
<link href="<?php echo base_url();?>assets/default.css" rel="stylesheet" type="text/css" media="all" />
<link href="<?php echo base_url();?>assets/fonts.css" rel="stylesheet" type="text/css" media="all" />

<!--[if IE 6]><link href="default_ie6.css" rel="stylesheet" type="text/css" /><![endif]-->
<style type="text/css">
    .boxA,
    .boxB,
    .boxC
    {
        width: 620px;
    }
    #banner
    {
        overflow: hidden;
        padding: 7em 0em 5em 0em;
        background: #202020;
        background-size: cover;
        text-align: center;
        background-image: url("https://i.pinimg.com/originals/8f/37/d5/8f37d513adc37648ac8dabc3e61310d5.jpg");
        color: rgba(255,255,255,.8);
    }
</style>

</head>
<body>
<div id="wrapper">
    <div id="header-wrapper">
    <div id="header" class="container">
        <div id="logo">
            <h1><a href="#">DOROREJO MOTOR</a></h1>
        </div>
        <div id="menu">
            <ul>
                <li class="current_page_item"><a href="<?php echo base_url();?>home" accesskey="1" title="">BERANDA</a></li>
                <li><a href="#" accesskey="2" title="">TENTANG KAMI</a></li>
                <li><a href="#" accesskey="3" title="">KONTAK KAMI</a></li>
            </ul>
        </div>
    </div>
    </div>
    <div id="banner">
        <div class="container">
            <div class="title">
                <h2 style="color: rgba(255,255,255,.6); text-shadow: 2px 2px #000000;">OROREJO MOTOR</h2>
                <span class="byline">SPAREPART TERLENGLENGKAP DI PEKALONGAN</span> </div>
            <ul class="actions">
                <!-- <li><a href="#" class="button">Etiam posuere</a></li> -->
            </ul>
        </div>
    </div>
    <div id="extra" class="container">
        <div class="title">
            <h2>Log <i>Proses</i></h2>
            <span class="byline">Berikut merupakan proses perhitungan :</span> </div>
        <br><br>
        <h3>Tabel Sparepart</h3>
        <table width="100%" border="1">
                    <tr>
                        <th>No.</th>
                        <th>Merk</th>
                        <th>Jenis</th>
                        <th>Sparepart</th>
                        <th>Harga</th>
                        <th>Garansi</th>
                    </tr>
            <?php $nono=1; foreach ($sparepart as $p) { ?>
                    <tr>
                        <td align="center"><?php echo $nono++ ?></td>
                        <td align="center"><?php if($p->merk == 5){ echo "Honda"; }elseif($p->merk == 4){echo "Yamaha";}elseif($p->merk == 3){echo "Kawasaki";}elseif($p->merk == 2){echo "Suzuki";}else{echo "Lainnya";} ?></td>
                        <td align="center"><?php echo $p->jenis ?></td>
                        <td align="center"><?php echo $p->sparepart ?></td>
                        <td align="center"><?php echo rupiah($p->harga); ?></td>
                        <td align="center"><?php echo $p->garansi." / Bulan"; ?></td>
                    </tr>
            <?php } ?>
        </table>
        <hr>
        <h3>Tabel Alternatif</h3>
        <table width="100%" border="1">
                    <tr>
                        <th>No.</th>
                        <th>Kode Alt</th>
                        <th>C1</th>
                        <th>C2</th>
                        <th>C3</th>
                        <th>C4</th>
                        <th>C5</th>
                    </tr>
            <?php $nomer=1; foreach ($alternatif as $a) { ?>
                    <tr>
                        <td align="center"><?php echo $nomer++ ?></td>
                        <td align="center"><?php echo $a->kode_alt ?></td>
                        <td align="center"><?php echo $a->C1 ?></td>
                        <td align="center"><?php echo $a->C2 ?></td>
                        <td align="center"><?php echo $a->C3 ?></td>
                        <td align="center"><?php echo $a->C4 ?></td>
                        <td align="center"><?php echo $a->C5 ?></td>
                    </tr>
            <?php } ?>
        </table>
        <hr>
        <h3>Tabel Normalisasi</h3>
        <table width="100%" border="1">
                    <tr>
                        <th>No.</th>
                        <th>Kode Alt</th>
                        <th>C1</th>
                        <th>C2</th>
                        <th>C3</th>
                        <th>C4</th>
                        <th>C5</th>
                    </tr>
            <?php $nome=1; foreach ($normalisasi as $n) { ?>
                    <tr>
                        <td align="center"><?php echo $nome++ ?></td>
                        <td align="center"><?php echo $n->kode_alt ?></td>
                        <td align="center"><?php echo $n->C1 ?></td>
                        <td align="center"><?php echo $n->C2 ?></td>
                        <td align="center"><?php echo $n->C3 ?></td>
                        <td align="center"><?php echo $n->C4 ?></td>
                        <td align="center"><?php echo $n->C5 ?></td>
                    </tr>
            <?php } ?>
        </table>
        <hr>
        <h3>Tabel Perangkingan</h3>
        <table width="100%" border="1">
                    <tr>
                        <th>No.</th>
                        <th>Kode Alt</th>
                        <th>Total(bobot * normalisasi)</th>
                    </tr>
            <?php $nom=1; foreach ($rangking as $r) { ?>
                    <tr>
                        <td align="center"><?php echo $nom++ ?></td>
                        <td align="center"><?php echo $r->kode_alt ?></td>
                        <td align="center"><?php echo $r->total_alt / 100; ?></td>
                    </tr>
            <?php } ?>
        </table>
    </div>
    <center><a href="<?php echo base_url();?>home">Kembali</a></center>
</div>
<div id="copyright" class="container">
    <p>&copy; All rights reserved. | by <a href="http://fotogrph.com/">DOROREJO MOTOR</a></p>
</div>
</body>
</html>
