<?php 
          function rupiah($angka){
    
            $hasil_rupiah = number_format($angka,2,',','.');
            return $hasil_rupiah;
         
        }
?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Strict//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-strict.dtd">
<!--
Design by TEMPLATED
http://templated.co
Released for free under the Creative Commons Attribution License

Name       : PlainDisplay 
Description: A two-column, fixed-width design with dark color scheme.
Version    : 1.0
Released   : 20140309

-->
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<title>Toko Sparepart</title>
<meta name="keywords" content="" />
<meta name="description" content="" />
<link href="http://fonts.googleapis.com/css?family=Varela" rel="stylesheet" />
<link href="<?php echo base_url();?>assets/default.css" rel="stylesheet" type="text/css" media="all" />
<link href="<?php echo base_url();?>assets/fonts.css" rel="stylesheet" type="text/css" media="all" />

<!--[if IE 6]><link href="default_ie6.css" rel="stylesheet" type="text/css" /><![endif]-->
<style type="text/css">
    .boxA,
    .boxB,
    .boxC
    {
        width: 620px;
    }
    #banner
    {
        overflow: hidden;
        padding: 7em 0em 5em 0em;
        background: #202020;
        background-size: cover;
        text-align: center;
        background-image: url("https://i.pinimg.com/originals/8f/37/d5/8f37d513adc37648ac8dabc3e61310d5.jpg");
        color: rgba(255,255,255,.8);
    }
</style>

</head>
<body>
<div id="wrapper">
    <div id="header-wrapper">
    <div id="header" class="container">
        <div id="logo">
            <h1><a href="#">DOROREJO MOTOR</a></h1>
        </div>
        <div id="menu">
            <ul>
                <li class="current_page_item"><a href="<?php echo base_url('home');?>" accesskey="1" title="">BERANDA</a></li>
                <li><a href="#" accesskey="2" title="">TENTANG KAMI</a></li>
                <li><a href="#" accesskey="3" title="">KONTAK KAMI</a></li>
            </ul>
        </div>
    </div>
    </div>
    <div id="banner">
        <div class="container">
            <div class="title">
                <h2 style="color: rgba(255,255,255,.6); text-shadow: 2px 2px #000000;">OROREJO MOTOR</h2>
                <span class="byline">SPAREPART TERLENGLENGKAP DI PEKALONGAN</span> </div>
            <ul class="actions">
                <!-- <li><a href="#" class="button">Etiam posuere</a></li> -->
            </ul>
        </div>
    </div>
    <div id="extra" class="container">
        <div class="title">
            <h2>Pencarian <i>Sparepart</i></h2>
            <span class="byline">Silahkan Isi di Kolom Pencarian dibawah :</span> </div>
        <div id="three-column">
            <div class="boxA">
                <div class="box">
                    <form action="<?php echo base_url('home/proses'); ?>" method="post">
                    <table width="100%">
                        <tr>
                            <td style="text-align: left;">Merk Motor</td>
                            <td>:</td>
                            <td>
                                <select name="merk" style="width: 247px;height: 30px;">
                                    <option selected="" disabled="">-- Pilih Merk Motor --</option>
                                    <option value="5">Honda</option>
                                    <option value="4">Yamaha</option>
                                    <option value="3">Kawasaki</option>
                                    <option value="2">Suzuki</option>
                                    <option value="1">Lainnya</option>
                                </select>
                            </td>
                        </tr>
                        <tr>
                            <td style="text-align: left;">Jenis Motor</td>
                            <td>:</td>
                            <td style="padding: 15px;">
                                <input type="text" name="jenis" style="width: 240px;height: 25px;" placeholder="Jenis Motor">
                            </td>
                        </tr>
                        <tr>
                            <td style="text-align: left;">Nama Sparepart</td>
                            <td>:</td>
                            <td>
                                <input type="text" name="sparepart" style="width: 240px;height: 25px;" placeholder="Nama Sparepart">
                            </td>
                        </tr>
                    </table>
                </div>
            </div>
        </div>
        <ul class="actions">
            <li><input type="submit" value="Cari" class="button" style="border-radius: 50px;"></li>
        </ul>
        </form>
        <br><br><br><br>
         <?php
                if (!empty($this->session->userdata('kode_alt'))) { 
                    $id_sparepart = explode('-', $this->session->userdata('kode_alt'));
                    $get_sparepart = $this->db->get_where('tbl_sparepart', array('id' => $id_sparepart[1]))->row();
                    ?>
                    <center>
                        <p>Ini merupakan Sparepart yang di rekomendasikan oleh sistem kami</p><hr>
                        <table width="100%" border="1">
                            <tr>
                                <th>Merk</th>
                                <th>Jenis</th>
                                <th>Sparepart</th>
                                <th>Harga</th>
                                <th>Garansi</th>
                            </tr>
                            <tr>
                                <td align="center"><?php if($get_sparepart->merk == 5){ echo "Honda"; }elseif($get_sparepart->merk == 4){echo "Yamaha";}elseif($get_sparepart->merk == 3){echo "Kawasaki";}elseif($get_sparepart->merk == 2){echo "Suzuki";}else{echo "Lainnya";}?></td>
                                <td align="center"><?php echo $get_sparepart->jenis ?></td>
                                <td align="center"><?php echo $get_sparepart->sparepart ?></td>
                                <td align="center"><?php echo rupiah($get_sparepart->harga); ?></td>
                                <td align="center"><?php echo $get_sparepart->garansi.' Bulan'; ?></td>
                            </tr>
                        </table>
                    </center>
            <?php        # code...
                }
            ?>
    </div>
    <center><a href="<?php echo base_url('home/log'); ?>">Lihat Log Proses</a></center>
</div>
<div id="copyright" class="container">
    <p>&copy; All rights reserved. | by <a href="http://fotogrph.com/">DOROREJO MOTOR</a></p>
</div>
</body>
</html>
