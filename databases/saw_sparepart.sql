-- phpMyAdmin SQL Dump
-- version 4.5.1
-- http://www.phpmyadmin.net
--
-- Host: 127.0.0.1
-- Generation Time: Jul 13, 2020 at 04:48 PM
-- Server version: 10.1.10-MariaDB
-- PHP Version: 5.5.30

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `saw_sparepart`
--

-- --------------------------------------------------------

--
-- Table structure for table `tbl_alternatif`
--

CREATE TABLE `tbl_alternatif` (
  `id` int(11) NOT NULL,
  `kode_alt` varchar(15) NOT NULL,
  `C1` int(5) NOT NULL,
  `C2` int(5) NOT NULL,
  `C3` int(5) NOT NULL,
  `C4` int(5) NOT NULL,
  `C5` int(5) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `tbl_alternatif`
--

INSERT INTO `tbl_alternatif` (`id`, `kode_alt`, `C1`, `C2`, `C3`, `C4`, `C5`) VALUES
(1, 'A-1', 5, 1, 1, 4, 1),
(2, 'A-2', 5, 1, 1, 4, 2),
(3, 'A-3', 5, 1, 1, 4, 2),
(4, 'A-4', 5, 1, 1, 4, 3),
(5, 'A-5', 4, 1, 1, 5, 1),
(6, 'A-6', 3, 1, 1, 5, 1),
(7, 'A-7', 4, 1, 1, 3, 1),
(8, 'A-8', 5, 2, 2, 5, 1),
(9, 'A-9', 4, 1, 1, 3, 1),
(10, 'A-10', 4, 1, 1, 4, 1);

-- --------------------------------------------------------

--
-- Table structure for table `tbl_crips`
--

CREATE TABLE `tbl_crips` (
  `id` int(11) NOT NULL,
  `kode_kriteria` varchar(5) NOT NULL,
  `nama_kriteria` varchar(50) NOT NULL,
  `crips` varchar(50) NOT NULL,
  `nilai` int(5) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `tbl_crips`
--

INSERT INTO `tbl_crips` (`id`, `kode_kriteria`, `nama_kriteria`, `crips`, `nilai`) VALUES
(1, 'C1', 'Merk', 'Lainnya', 1),
(2, 'C1', 'Merk', 'Suzuki', 2),
(3, 'C1', 'Merk', 'Kawazaki', 3),
(4, 'C1', 'Merk', 'Yamaha', 4),
(5, 'C1', 'Merk', 'Honda', 5),
(6, 'C2', 'Jenis Motor', 'Tidak Sesuai', 1),
(7, 'C2', 'Jenis Motor', 'Sesuai', 2),
(8, 'C3', 'Jenis Sparepart', 'Tidak Sesuai', 1),
(9, 'C3', 'Jenis Sparepart', 'Sesuai', 2),
(10, 'C4', 'Harga', 'C4 <= 25000', 1),
(11, 'C4', 'Harga', '25000 < C4 <= 50000', 2),
(12, 'C4', 'Harga', '50000 < C4 <= 100000', 3),
(13, 'C4', 'Harga', '100000 < C4 <= 200000', 4),
(14, 'C4', 'Harga', 'C4 > 200000', 5),
(15, 'C5', 'Garansi', 'C5 <= 2', 1),
(16, 'C5', 'Garansi', 'C5 = 3', 2),
(17, 'C5', 'Garansi', 'C5 = 4', 3),
(18, 'C5', 'Garansi', 'C5 = 5', 4),
(19, 'C5', 'Garansi', 'C5 > 5', 5);

-- --------------------------------------------------------

--
-- Table structure for table `tbl_kriteria`
--

CREATE TABLE `tbl_kriteria` (
  `id` int(11) NOT NULL,
  `kode_kriteria` varchar(5) NOT NULL,
  `nama_kriteria` varchar(50) NOT NULL,
  `atribut` int(2) NOT NULL,
  `bobot` int(5) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `tbl_kriteria`
--

INSERT INTO `tbl_kriteria` (`id`, `kode_kriteria`, `nama_kriteria`, `atribut`, `bobot`) VALUES
(1, 'C1', 'Merk', 2, 25),
(2, 'C2', 'Jenis Motor', 2, 20),
(3, 'C3', 'Jenis Sparepart', 2, 15),
(4, 'C4', 'Harga', 1, 30),
(6, 'C5', 'Garansi', 2, 10);

-- --------------------------------------------------------

--
-- Table structure for table `tbl_menu`
--

CREATE TABLE `tbl_menu` (
  `id` int(11) NOT NULL,
  `menu` varchar(50) NOT NULL,
  `parent` int(2) NOT NULL,
  `url` varchar(50) NOT NULL,
  `icon` varchar(50) NOT NULL,
  `user_level` int(1) NOT NULL,
  `set_active` int(1) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data for table `tbl_menu`
--

INSERT INTO `tbl_menu` (`id`, `menu`, `parent`, `url`, `icon`, `user_level`, `set_active`) VALUES
(1, 'Dashboard', 0, 'dashboard', 'fas fa-fw fa-tachometer-alt', 1, 1),
(2, 'Kriteria', 0, '#', 'fas fa-fw fa-folder', 1, 1),
(3, 'Data Kriteria', 2, 'kriteria', '', 1, 1),
(4, 'Sparepart', 0, '#', 'fas fa-fw fa-folder', 1, 1),
(5, 'Daftar Sparepart', 4, 'sparepart', '', 1, 1);

-- --------------------------------------------------------

--
-- Table structure for table `tbl_normalisasi`
--

CREATE TABLE `tbl_normalisasi` (
  `id` int(11) NOT NULL,
  `kode_alt` varchar(15) NOT NULL,
  `C1` float NOT NULL,
  `C2` float NOT NULL,
  `C3` float NOT NULL,
  `C4` float NOT NULL,
  `C5` float NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `tbl_normalisasi`
--

INSERT INTO `tbl_normalisasi` (`id`, `kode_alt`, `C1`, `C2`, `C3`, `C4`, `C5`) VALUES
(1, 'A-1', 1, 0.5, 0.5, 1.33333, 0.333333),
(2, 'A-2', 1, 0.5, 0.5, 1.33333, 0.666667),
(3, 'A-3', 1, 0.5, 0.5, 1.33333, 0.666667),
(4, 'A-4', 1, 0.5, 0.5, 1.33333, 1),
(5, 'A-5', 0.8, 0.5, 0.5, 1.66667, 0.333333),
(6, 'A-6', 0.6, 0.5, 0.5, 1.66667, 0.333333),
(7, 'A-7', 0.8, 0.5, 0.5, 1, 0.333333),
(8, 'A-8', 1, 1, 1, 1.66667, 0.333333),
(9, 'A-9', 0.8, 0.5, 0.5, 1, 0.333333),
(10, 'A-10', 0.8, 0.5, 0.5, 1.33333, 0.333333);

-- --------------------------------------------------------

--
-- Table structure for table `tbl_pencarian`
--

CREATE TABLE `tbl_pencarian` (
  `id` int(11) NOT NULL,
  `merk` int(1) NOT NULL,
  `jenis` varchar(30) NOT NULL,
  `sparepart` varchar(30) NOT NULL,
  `is_finish` int(1) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `tbl_pencarian`
--

INSERT INTO `tbl_pencarian` (`id`, `merk`, `jenis`, `sparepart`, `is_finish`) VALUES
(1, 5, 'Supra X', 'Ban', 0);

-- --------------------------------------------------------

--
-- Table structure for table `tbl_ranking`
--

CREATE TABLE `tbl_ranking` (
  `id` int(11) NOT NULL,
  `kode_alt` varchar(50) NOT NULL,
  `total_alt` float NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `tbl_ranking`
--

INSERT INTO `tbl_ranking` (`id`, `kode_alt`, `total_alt`) VALUES
(1, 'A-1', 70.8333),
(2, 'A-2', 70.8333),
(3, 'A-3', 70.8333),
(4, 'A-4', 70.8333),
(5, 'A-5', 69.1667),
(6, 'A-6', 64.1667),
(7, 'A-7', 62.5),
(8, 'A-8', 106.667),
(9, 'A-9', 62.5),
(10, 'A-10', 65.8333);

-- --------------------------------------------------------

--
-- Table structure for table `tbl_sparepart`
--

CREATE TABLE `tbl_sparepart` (
  `id` int(11) NOT NULL,
  `merk` int(11) NOT NULL,
  `jenis` varchar(20) NOT NULL,
  `sparepart` varchar(60) NOT NULL,
  `harga` varchar(10) NOT NULL,
  `garansi` int(2) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `tbl_sparepart`
--

INSERT INTO `tbl_sparepart` (`id`, `merk`, `jenis`, `sparepart`, `harga`, `garansi`) VALUES
(1, 5, 'Beat', 'Rantai', '160000', 2),
(2, 5, 'Beat', 'Rantai 415', '160000', 3),
(3, 5, 'Beat', 'Rantai 420', '185000', 3),
(4, 5, 'Beat', 'Rantai 428', '190000', 4),
(5, 4, 'N-Max', 'Rantai 428', '2250000', 2),
(6, 3, 'Ninja 250', 'Ban', '240000', 1),
(7, 4, 'Jupiter', 'Lampu Depan', '80000', 1),
(8, 5, 'Supra X', 'Ban', '225000', 1),
(9, 4, 'Jupiter MX', 'Lampu Belakang', '60000', 1),
(10, 4, 'Jupiter', 'Body Depan', '125000', 1);

-- --------------------------------------------------------

--
-- Table structure for table `tbl_user`
--

CREATE TABLE `tbl_user` (
  `id` int(11) NOT NULL,
  `username` varchar(25) NOT NULL,
  `fullname` varchar(60) NOT NULL,
  `password` varchar(50) NOT NULL,
  `level` int(1) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data for table `tbl_user`
--

INSERT INTO `tbl_user` (`id`, `username`, `fullname`, `password`, `level`) VALUES
(1, 'admin', 'Candika Abdi Nugroho', '62cc2d8b4bf2d8728120d052163a77df', 1);

--
-- Indexes for dumped tables
--

--
-- Indexes for table `tbl_alternatif`
--
ALTER TABLE `tbl_alternatif`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `tbl_crips`
--
ALTER TABLE `tbl_crips`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `tbl_kriteria`
--
ALTER TABLE `tbl_kriteria`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `tbl_menu`
--
ALTER TABLE `tbl_menu`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `tbl_normalisasi`
--
ALTER TABLE `tbl_normalisasi`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `tbl_pencarian`
--
ALTER TABLE `tbl_pencarian`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `tbl_ranking`
--
ALTER TABLE `tbl_ranking`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `tbl_sparepart`
--
ALTER TABLE `tbl_sparepart`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `tbl_user`
--
ALTER TABLE `tbl_user`
  ADD PRIMARY KEY (`id`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `tbl_alternatif`
--
ALTER TABLE `tbl_alternatif`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=11;
--
-- AUTO_INCREMENT for table `tbl_crips`
--
ALTER TABLE `tbl_crips`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=20;
--
-- AUTO_INCREMENT for table `tbl_kriteria`
--
ALTER TABLE `tbl_kriteria`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=7;
--
-- AUTO_INCREMENT for table `tbl_menu`
--
ALTER TABLE `tbl_menu`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=6;
--
-- AUTO_INCREMENT for table `tbl_normalisasi`
--
ALTER TABLE `tbl_normalisasi`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=11;
--
-- AUTO_INCREMENT for table `tbl_pencarian`
--
ALTER TABLE `tbl_pencarian`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;
--
-- AUTO_INCREMENT for table `tbl_ranking`
--
ALTER TABLE `tbl_ranking`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=11;
--
-- AUTO_INCREMENT for table `tbl_sparepart`
--
ALTER TABLE `tbl_sparepart`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=11;
--
-- AUTO_INCREMENT for table `tbl_user`
--
ALTER TABLE `tbl_user`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
